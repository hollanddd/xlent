import xlrd

class Xlent:
  def __init__(self, filename):
    self.filename = filename
    self.book = xlrd.open_workbook(filename)
    self.encoding = self.book.encoding
  
  def sheet_names(self):
    return self.book.sheet_names()
  
  def number_of_sheets(self):
    return self.book.nsheets()

  def sheets(self):
    return self.book.sheets()
  
  def sheet_attrs(self):
    sheets = self.sheets()
    attribute_list = []
    for sheet in sheets:
      count = 0
      data = { 'index' : count, 'attrs' : (str(sheet.name), sheet.ncols, sheet.nrows) }
      attribute_list.append(data)
      count += 1
    return attribute_list
  
  def get_value_of_cell(self, cell, zero_bool=False, rnd=None):
    # zero_bool flag is used to call _get_cowboy_bool
    # cowboy_bool examples: 0 for False, 1 for True
    # y for True, n for False and their variations
    if cell.ctype == xlrd.XL_CELL_EMPTY or cell.ctype == xlrd.XL_CELL_BLANK or cell.value is None:
      return None
    elif cell.ctype == xlrd.XL_CELL_BOOLEAN:
      return self._get_bool_value(cell)
    elif cell.ctype == xlrd.XL_CELL_NUMBER:
      if zero_bool == False or float(cell.value > 1):
        return self._get_float_value(cell, rnd)
      else:
        return self._get_cowboy_bool(cell)
    elif cell.ctype == xlrd.XL_CELL_TEXT:
      try:
        float(cell.value)
        return self._get_float_value(cell, rnd)
      except ValueError:
        return self._get_string_value(cell)
    elif zero_bool and (cell.ctype == xlrd.XL_CELL_TEXT or cell.ctype == xlrd.XL_CELL_NUMBER):
      return self._get_cowboy_bool(cell)
  
  def _get_string_value(self, cell):
    if cell.ctype == xlrd.XL_CELL_TEXT:
      return cell.value.encode('utf-8')
    else:
      return cell.value
  
  def _get_bool_value(self, cell):
    if cell.ctype == xlrd.XL_CELL_BOOLEAN:
      return cell.value
  
  def _get_cowboy_bool(self, cell):
    # examples: 
    # 0 for False, 1 for True
    # y for True, n for False 
    # and their variations
    ret = False
    if cell.ctype == xlrd.XL_CELL_NUMBER:
      if cell.value == 0:
        ret = False
      elif cell.value == 1:
        ret = True
    elif cell.ctype == xlrd.XL_CELL_TEXT:
      val = self._get_string_value(cell)
      if val == '1' or val == 't' or val == 'y' \
         or val == 'true' or val == 'yes':
         ret = True
    return ret
  
  def _get_float_value(self, cell, rnd=None):
    ret = cell
    if cell.ctype == xlrd.XL_CELL_NUMBER:
      ret = float(cell.value)
    elif cell.ctype == xlrd.XL_CELL_TEXT:
      try:
        ret = float(cell.value)
      except ValueError:
        ret = cell
    if ret and ret != cell and rnd is not None:
      ret = round(ret, rnd)
    return ret
  
    def _attrs_to_screen(self):
      # not intended for use ouside of
      # debugging and logging
      attributes = self.sheet_attrs()
      for attribute in attributes:
        index = attribute['index']
        name, cols, rows = attribute['attrs']
        print 'index: %(index)s , sheet name: %(name)s, ncols: %(cols)s, nrows: %(rows)s' % \
              {'index': index, 'name': name, 'cols': cols, 'rows': rows }
  