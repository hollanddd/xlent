import sys, os.path, unittest
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from lib.xlent import Xlent

class XlentTest(unittest.TestCase):
  def setUp(self):
    self.filename = os.path.join(os.path.dirname(__file__), 'test.xls')
    self.its = Xlent(self.filename)
    self.sheet = self.its.sheets()[0]
  
  def testHasFilename(self):
    self.failIf(self.its.filename != self.filename)
  
  def testHasWorkbook(self):
    self.failIf(self.its.book == False)
  
  def testKnowsNumberOfSheetsInBook(self):
    self.failIf(self.its.number_of_sheets == 0)
  
  def testKnowsNamesOfSheetsInBook(self):
    sheet_names = self.its.sheet_names()
    self.failIf(type(sheet_names) != list)
    self.failIf(sheet_names[0] != 'TBL14')
  
  def testKnowsItsEncoding(self):
    self.assertEqual(self.its.encoding, 'utf_16_le')
  
  def testCanHazSheets(self):
    sheet_names = self.its.sheet_names()
    sheets = self.its.sheets()
    self.failIf(sheet_names[0] != self.sheet.name)
  
  def testHasSheetData(self):
    attributes = self.its.sheet_attrs()
    for attribute in attributes:
      self.assertEqual(attribute['index'], 0)
      self.assertEqual(attribute['attrs'], (str(self.sheet.name), self.sheet.ncols, self.sheet.nrows))

  def testCanDisplaySheetData(self):
    # has no return as it displays to the screen
    self.assertEqual(self.its._attrs_to_screen(), None)
  
  def testGetStringValue(self):
    valid_cell = self.sheet.cell(4, 65)
    self.assertEqual(type(self.its._get_string_value(valid_cell)), str)
    self.assertEqual(self.its._get_string_value(valid_cell), 'Net gain')
  
  def testGetBoolValue(self):
    # testing with float for cowbwoy bool
    bool_cell = self.sheet.cell(24, 133)
    # cell.type is not bool so None is returned
    self.assertEqual(self.its._get_bool_value(bool_cell), None)
    # bool_cell is float(0.0). cowboy_bool converts floats
    # and strings to bool value.
    self.assertEqual(self.its._get_cowboy_bool(bool_cell), False)
    self.assertEqual(type(self.its._get_cowboy_bool(bool_cell)), bool)
  
  def testGetFloatValue(self):
    valid_cell = self.sheet.cell(9, 1)
    self.assertEqual(self.its._get_float_value(valid_cell), 3210252.0)
  
  def testStringFromGetValueOfCell(self):
    string_cell = self.sheet.cell(4, 65)  
    self.assertEqual(self.its.get_value_of_cell(string_cell), 'Net gain')
    self.assertEqual(type(self.its.get_value_of_cell(string_cell)), str)
  
  def testBoolFromGetValueOfCell(self):
    bool_cell = self.sheet.cell(24, 133)
    # passing with zero_bool=True
    self.assertEqual(self.its.get_value_of_cell(bool_cell, True), False)
    self.assertEqual(type(self.its.get_value_of_cell(bool_cell, True)), bool)
    # passing with zero_bool defaulted to false 
    # return value of 0.0 will remain untouched
    self.assertEqual(self.its.get_value_of_cell(bool_cell), 0.0)
  
  def testFloatFromGetValueOfCell(self):
    float_cell = self.sheet.cell(9, 1)
    self.assertEqual(self.its.get_value_of_cell(float_cell), 3210252.0)
    self.assertEqual(type(self.its.get_value_of_cell(float_cell)), float)
  
  def testEmptyFromGetValueOfCell(self):
    empty_cell = self.sheet.cell(44, 1)
    self.assertEqual(self.its.get_value_of_cell(empty_cell), None)
  
  def testCanReadRowsFromSheets(self):
    number_of_rows = self.sheet.nrows
    number_of_cols = self.sheet.ncols
    at_row = 0
    at_col = 0
    print '\n'
    while at_row < number_of_rows:
      while at_col < number_of_cols:
        cell = self.sheet.cell(at_row, at_col)
        value = self.its.get_value_of_cell(cell, True)
        if value:
          print '(' + str(at_row) +', ' + str(at_col) + '): ' + str(value),
        at_col += 1
      print '\n'
      at_col = 0
      at_row += 1
    pass
  
def main():
  unittest.main()

if __name__ == '__main__':
	main()
